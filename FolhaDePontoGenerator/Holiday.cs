﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FolhaDePontoGenerator
{
    public class Holiday
    {
        public string Date { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Type_code { get; set; }
    }
}
