﻿using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Linq;
using OfficeOpenXml.Style;
using System.Drawing;

namespace FolhaDePontoGenerator
{
    class Program
    {
        private static bool _isTrainee = false;
        private static int _year = 2019;
        private static string _state = "SP";
        private static string _city = "BARUERI";
        private static string _token = "amhvbm0ubW9saW5hQGdtYWlsLmNvbSZoYXNoPTE0OTQ2NzQ0MQ;";
        //Website to generate tokens http://www.calendario.com.br/api_feriados_municipais_estaduais_nacionais.php
        private static string _holidaysAPIUrl = $"https://api.calendario.com.br?json=true&ano={_year}&estado={_state}&cidade={_city}&token={_token}";
        private static string _filePath = $@"c:\workbooks\FloraDePonto_{_year}.xlsx";

        static void Main(string[] args)
        {
            var holidays = GetHolidays();

            if (holidays.Count == 0) return;

            var file = new FileInfo(_filePath);

            if(file != null && file.Exists)
            {
                file.Delete();
            }

            using (var p = new ExcelPackage(file))
            {
                int monthCount = 0;
                foreach (var month in DateTimeFormatInfo.CurrentInfo.MonthNames.Where(w=>!string.IsNullOrEmpty(w)))
                {
                    monthCount++;
                    var ws = p.Workbook.Worksheets.Add(month);

                    CreateHeader(ws);

                    var daysOfMonth = GetWeekdatesandDates(monthCount, _year).OrderBy(o=>o).ToList();

                    var dayCount = 0;
                    foreach (var day in daysOfMonth)
                    {
                        dayCount++;
                        ws.Cells[dayCount + 1, 1].Value = $"{day.Day} - {day.DayOfWeek.ToString()}";

                        var holiday = holidays.FirstOrDefault(f => f.Type.StartsWith("Feriado") && f.Date == $"{day.Day.ToString("00")}/{monthCount.ToString("00")}/{_year}");

                        if(holiday != null)
                        {
                            ws.Cells[dayCount + 1, 1].Style.Font.Bold = true;
                            ws.Cells[dayCount + 1, 1].Style.Font.Color.SetColor(Color.Red);
                            ws.Cells[dayCount + 1, 6].Value = holiday.Type;
                            ws.Cells[dayCount + 1, 7].Value = holiday.Name;
                            ws.Cells[dayCount + 1, 7].AddComment(holiday.Description, "John");
                        }

                        var random = new Random();
                        var randomStartMultiplier = random.Next(-1, 1) == 0 ? -1 : 1;
                        var randomLunchMultiplier = random.Next(-1, 1) == 0 ? -1 : 1;
                        
                        var randomStartTime = random.Next(0, 15);
                        var randomLunchTime = random.Next(0, 15);
                        var startTime = new TimeSpan(9, randomStartTime * randomStartMultiplier, 0);
                        var lunchTime = new TimeSpan(12, randomLunchTime * randomLunchMultiplier, 0);

                        ws.Cells[dayCount + 1, 2].Value = IsWeekEnd(day) || holiday != null ? "-" : startTime.ToString("hh\\:mm");
                        ws.Cells[dayCount + 1, 3].Value = IsWeekEnd(day) || holiday != null ? "-" : lunchTime.ToString("hh\\:mm");
                        ws.Cells[dayCount + 1, 4].Value = IsWeekEnd(day) || holiday != null ? "-" : lunchTime.Add(TimeSpan.FromHours(1)).ToString("hh\\:mm");
                        ws.Cells[dayCount + 1, 5].Value = IsWeekEnd(day) || holiday != null ? "-" : startTime.Add(TimeSpan.FromHours(_isTrainee ? 7 : 9)).ToString("hh\\:mm");
                    }
                }

                p.Save();
            }

            Console.WriteLine("Sucesso!!! ;)");
        }

        private static bool IsWeekEnd(DateTime day)
        {
            return day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday;
        }

        private static List<Holiday> GetHolidays()
        {
            try
            {
                return JsonConvert.DeserializeObject<List<Holiday>>(new HttpClient().GetAsync(_holidaysAPIUrl).Result.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new List<Holiday>();
        }

        public static List<DateTime> GetWeekdatesandDates(int Month, int Year)
        {
            List<DateTime> weekdays = new List<DateTime>();

            DateTime firstOfMonth = new DateTime(Year, Month, 1);

            DateTime currentDay = firstOfMonth;
            while (firstOfMonth.Month == currentDay.Month)
            {
                DayOfWeek dayOfWeek = currentDay.DayOfWeek;
                //if (dayOfWeek != DayOfWeek.Saturday && dayOfWeek != DayOfWeek.Sunday)
                    weekdays.Add(currentDay);

                currentDay = currentDay.AddDays(1);
            }

            return weekdays;
        }

        private static void CreateHeader(ExcelWorksheet ws)
        {
            ws.Cells["A1"].Value = "Dia";
            ws.Cells["B1"].Value = "Entrada";
            ws.Cells["C1"].Value = "Almoço";
            ws.Cells["D1"].Value = "Retorno";
            ws.Cells["E1"].Value = "Saída";
            ws.Cells["F1"].Value = "Tipo Feriado";
            ws.Cells["G1"].Value = "Feriado";

            //Settings Style
            var range = ws.Cells[1, 1, 32, 7];
            var table = ws.Tables.Add(range, $"table{ws.Name}");
            table.TableStyle = TableStyles.Light2;
        }
    }
}
